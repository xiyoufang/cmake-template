//
// Created by farmer on 2020/6/3.
//
#include "add.h"
#include <stdio.h>

int main() {
    int a = 10;
    int b = 8;

    printf("%d + %d = %d\n", a, b, add(a, b));

}